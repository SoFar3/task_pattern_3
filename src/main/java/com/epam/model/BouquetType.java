package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public enum BouquetType {

    PRIDE("Pride", OccasionType.WEDDING),
    VANILLA_SKY("Vanilla Sky", OccasionType.WEDDING),
    MARSA_ALAM("Marsa Alam", OccasionType.BIRTHDAY),
    HILLIER("Hillier", OccasionType.BIRTHDAY),
    LOVE("Love", OccasionType.VALENTINE),
    BEAUTY("Beauty", OccasionType.VALENTINE),
    MOURNING_BASKET_OF_RED_ROSES("Mourning Basket of Red Roses", OccasionType.FUNERAL);

    private String name;
    private OccasionType occasion;

    BouquetType(String name, OccasionType occasion) {
        this.name = name;
        this.occasion = occasion;
    }

    public static List<BouquetType> getByOccasion(OccasionType type) {
        List<BouquetType> bouquetTypeList = new ArrayList<>();
        for (BouquetType value : values()) {
            if (value.occasion.equals(type)) {
                bouquetTypeList.add(value);
            }
        }
        return bouquetTypeList;
    }

    @Override
    public String toString() {
        return name;
    }

}
