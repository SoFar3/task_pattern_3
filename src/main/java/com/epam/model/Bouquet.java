package com.epam.model;

import java.util.HashMap;
import java.util.Map;

public class Bouquet {

    // Integer - amount of flowers in bouquet
    private Map<FlowerType, Integer> flowers;
    private WrappingType wrappingType;

    public Bouquet() {
        flowers = new HashMap<>();
    }

    public void addFlower(FlowerType flower, int amount) {
        flowers.put(flower, amount);
    }

    public double getPrice() {
        return flowers
                .entrySet()
                .stream()
                .mapToDouble(e ->
                        FlowerType.getPrice(e.getKey()) * e.getValue())
                .sum() + WrappingType.getPrice(wrappingType);
    }

    public Map<FlowerType, Integer> getFlowers() {
        return flowers;
    }

    public void setFlowers(Map<FlowerType, Integer> flowers) {
        this.flowers = flowers;
    }

    public WrappingType getWrappingType() {
        return wrappingType;
    }

    public void setWrappingType(WrappingType wrappingType) {
        this.wrappingType = wrappingType;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "flowers=" + flowers +
                ", wrappingType=" + wrappingType +
                ", price=" + getPrice() +
                '}';
    }
    
}
