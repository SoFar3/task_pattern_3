package com.epam.model;

public enum WrappingType {

    KRAFT_PAPER("Kraft paper", 7),
    ROPES_AND_RIBBONS("Ropes and ribbons", 9.5),
    SACKCLOTH("Sackcloth", 11),
    CLOTH("Cloth", 10),
    BAG("Bag", 6);

    private double price;
    private String name;

    WrappingType(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public static double getPrice(WrappingType type) {
        for (WrappingType value : values()) {
            if (type.name.equals(value.name)) {
                return value.price;
            }
        }
        return 0.0;
    }

    @Override
    public String toString() {
        return name;
    }

}
