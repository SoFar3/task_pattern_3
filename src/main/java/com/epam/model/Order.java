package com.epam.model;

public class Order {

    private Bouquet bouquet;
    private DeliveryType deliveryType;

    public Order() {
    }

    public Order(Bouquet bouquet, DeliveryType deliveryType) {
        this.bouquet = bouquet;
        this.deliveryType = deliveryType;
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public double getPrice() {
        return bouquet.getPrice() + DeliveryType.getPrice(deliveryType);
    }

    @Override
    public String toString() {
        return "Order{" +
                "bouquet=" + bouquet +
                ", deliveryType=" + deliveryType +
                '}';
    }

}
