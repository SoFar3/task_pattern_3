package com.epam.model;

public enum DeliveryType {

    PICKUP("Pickup", 0),
    POST("Post", 10);

    private double price;
    private String name;

    DeliveryType(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public static double getPrice(DeliveryType type) {
        for (DeliveryType value : values()) {
            if (type.name.equals(value.name)) {
                return type.price;
            }
        }
        return 0.0;
    }

    @Override
    public String toString() {
        return name;
    }

}
