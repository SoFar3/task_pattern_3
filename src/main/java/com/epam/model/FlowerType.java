package com.epam.model;

public enum FlowerType {

    LILAC("Lilac", 2.5),
    PURPLE_TULIP("Purple tulip", 3),
    NARCISSUS("Narcissus", 2),
    DAISY("Daisy", 2),
    MARIGOLD("Marigold", 2),
    BLACK_ROSE("Black rose", 5.5),
    RED_ROSE("Red rose", 4),
    GARDENIA("Gardenia", 2.7),
    CALLA_LILY("Calla lily", 4),
    MAGNOLIA("Magnolia", 3.2);

    private double price;
    private String name;

    FlowerType(String name, double price) {
        this.name = name;
        this.price = price;
    }

    /*public double getPrice() {
        for (FlowerType value : values()) {
            if (value.toString().equals(this.toString())) {
                return this.price;
            }
        }
        return 0.0;
    }*/

    public static double getPrice(FlowerType type) {
        for (FlowerType value : values()) {
            if (type.name.equals(value.name)) {
                return type.price;
            }
        }
        return 0.0;
    }

    @Override
    public String toString() {
        return name;
    }

}
