package com.epam.model.factory;

import com.epam.model.Bouquet;
import com.epam.model.BouquetType;
import com.epam.model.FlowerType;
import com.epam.model.WrappingType;

public class BouquetFactory {

    public static Bouquet getBouquet(BouquetType type) {
        Bouquet bouquet = new Bouquet();

        switch (type) {
            case LOVE:
                bouquet.addFlower(FlowerType.RED_ROSE, 101);
                bouquet.setWrappingType(WrappingType.KRAFT_PAPER);
                break;
            case BEAUTY:
                bouquet.addFlower(FlowerType.DAISY, 10);
                bouquet.addFlower(FlowerType.GARDENIA, 3);
                bouquet.setWrappingType(WrappingType.ROPES_AND_RIBBONS);
                break;
            case MOURNING_BASKET_OF_RED_ROSES:
                bouquet.addFlower(FlowerType.RED_ROSE, 10);
                bouquet.addFlower(FlowerType.BLACK_ROSE, 10);
                bouquet.setWrappingType(WrappingType.SACKCLOTH);
                break;
            case HILLIER:
                bouquet.addFlower(FlowerType.DAISY, 5);
                bouquet.addFlower(FlowerType.MAGNOLIA, 5);
                bouquet.addFlower(FlowerType.MARIGOLD, 5);
                bouquet.setWrappingType(WrappingType.BAG);
                break;
            case MARSA_ALAM:
                bouquet.addFlower(FlowerType.PURPLE_TULIP, 3);
                bouquet.addFlower(FlowerType.DAISY, 5);
                bouquet.setWrappingType(WrappingType.KRAFT_PAPER);
                break;
            case VANILLA_SKY:
                bouquet.addFlower(FlowerType.LILAC, 10);
                bouquet.addFlower(FlowerType.CALLA_LILY, 5);
                bouquet.addFlower(FlowerType.DAISY, 5);
                bouquet.setWrappingType(WrappingType.CLOTH);
                break;
            case PRIDE:
                bouquet.addFlower(FlowerType.GARDENIA, 1);
                bouquet.addFlower(FlowerType.CALLA_LILY, 1);
                bouquet.addFlower(FlowerType.MAGNOLIA, 1);
                bouquet.addFlower(FlowerType.LILAC, 1);
                bouquet.addFlower(FlowerType.PURPLE_TULIP, 5);
                bouquet.setWrappingType(WrappingType.ROPES_AND_RIBBONS);
                break;
        }

        return bouquet;
    }

}
