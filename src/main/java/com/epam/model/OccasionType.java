package com.epam.model;

public enum OccasionType {

    WEDDING("Wedding"),
    BIRTHDAY("Birthday"),
    VALENTINE("Valentine's Day"),
    FUNERAL("Funeral");

    private String name;

    OccasionType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
