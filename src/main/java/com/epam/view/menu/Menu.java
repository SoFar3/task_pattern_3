package com.epam.view.menu;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Menu {

    private int repeat;
    private Menu parent;
    private Map<String, Option> menu;

    public Menu() {
        this.parent = null;
        this.menu = new LinkedHashMap<>();
        this.repeat = 3;
    }

    public Menu(int repeat) {
        this();
        this.repeat = repeat;
    }

    public Menu(Menu parent) {
        this();
        this.parent = parent;
    }

    public Menu(Menu parent, int repeat) {
        this(parent);
        this.repeat = repeat;
    }

    public void addMenuOption(String key, Option option) {
        menu.put(key, option);
    }

    public Option getOption(String key) {
        return menu.get(key);
    }

    public Map<String, Option> getMenu() {
        return menu.entrySet()
                .stream()
                .filter(e -> e.getValue().getCondition())
                .collect(Collectors
                        .toMap(Map.Entry::getKey,
                                Map.Entry::getValue,
                                (k, v) -> k,
                                LinkedHashMap::new));
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public Menu getParent() {
        return parent;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "repeat=" + repeat +
                ", menu=" + menu +
                '}';
    }

}
