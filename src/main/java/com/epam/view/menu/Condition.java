package com.epam.view.menu;

public interface Condition {

    boolean test();

}
