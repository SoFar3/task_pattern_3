package com.epam.controller;

import com.epam.model.*;
import com.epam.model.factory.BouquetFactory;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import com.epam.view.menu.Menu;
import com.epam.view.menu.Option;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicReference;

public class ControllerImpl implements Controller {

    private View view;

    private static Logger logger = LogManager.getLogger();

    private Order order;

    public ControllerImpl() {
        view = new ViewImpl();
        order = new Order();
    }

    @Override
    public void start() {
        Menu mainMenu = new Menu(Integer.MAX_VALUE);

        AtomicReference<Bouquet> bouquet = new AtomicReference<>();

        mainMenu.addMenuOption("1", new Option("Show bouquets", () -> {
            Menu bouquetFactoryMenu = new Menu(mainMenu, 1);
            occasionMenu(bouquetFactoryMenu);
            view.setMenu(bouquetFactoryMenu);
        }));
        mainMenu.addMenuOption("2", new Option("Custom bouquet", () -> {
            Menu customBouquet = new Menu(mainMenu, Integer.MAX_VALUE);
            flowerMenu(customBouquet, new Bouquet());
            view.setMenu(customBouquet);
        }));
        mainMenu.addMenuOption("3", new Option("Ordered bouquet", () -> {
            View.print(order.toString());
        }));

        mainMenu.addMenuOption("Q", new Option("Quit", () -> System.exit(0)));

        view.setMenu(mainMenu);
        view.showMenu();
    }

    private void occasionMenu(Menu menu) {
        int counter = 1;
        for (OccasionType value : OccasionType.values()) {
            menu.addMenuOption((counter++) + "", new Option(value.toString(), () -> {
                Menu subMenu = new Menu(menu.getParent(), 1);
                bouquetMenu(subMenu, value);
                view.setMenu(subMenu);
            }));
        }
        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    private void bouquetMenu(Menu menu, OccasionType occasion) {
        int counter = 1;
        for (BouquetType bouquetType : BouquetType.getByOccasion(occasion)) {
            Bouquet bouquet = BouquetFactory.getBouquet(bouquetType);
            menu.addMenuOption((counter++) + "", new Option(bouquetType.toString() + " " + bouquet.getPrice(),
                    () -> {
                        order.setBouquet(bouquet);
                        Menu delivery = new Menu(menu.getParent(), 1);
                        deliveryMenu(delivery);
                        view.setMenu(delivery);
            }));
        }
        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    private void deliveryMenu(Menu menu) {
        int counter = 1;
        for (DeliveryType deliveryType : DeliveryType.values()) {
            menu.addMenuOption((counter++) + "", new Option(deliveryType.toString() + " " + DeliveryType.getPrice(deliveryType),
                    () -> order.setDeliveryType(deliveryType)));
        }
        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    /*private void customBouquetMenu(Menu menu) {


        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }*/

    private void flowerMenu(Menu menu, Bouquet bouquet) {
        int counter = 1;
        for (FlowerType value : FlowerType.values()) {
            menu.addMenuOption((counter++) + "", new Option(value.toString() + " " + FlowerType.getPrice(value), () -> {
                Integer flowerAmount = view.getInteger("Enter amount of flowers: ");
                bouquet.addFlower(value, flowerAmount);
            }));
        }
        menu.addMenuOption("W", new Option("Choose wrapping", () -> {
            Menu subMenu = new Menu(menu, 1);
            wrappingMenu(subMenu, bouquet);
            view.setMenu(subMenu);
        }));

        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

    private void wrappingMenu(Menu menu, Bouquet bouquet) {
        int counter = 1;
        for (WrappingType value : WrappingType.values()) {
            menu.addMenuOption((counter++) + "", new Option(value.toString() + " " + WrappingType.getPrice(value), () -> {
                bouquet.setWrappingType(value);
                order.setBouquet(bouquet);

                Menu delivery = new Menu(menu.getParent(), 1);
                deliveryMenu(delivery);
                view.setMenu(delivery);
            }));
        }
        menu.addMenuOption("Q", new Option("Quit", () -> view.setMenu(menu.getParent())));
    }

}
